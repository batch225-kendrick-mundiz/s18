
// Functions as Arguments

// Function parameters can also accept other functions as arguments.


function argumentFunction(){
    console.log("This function was passed as an argument before the message was printed.")
}

function invokeFunction(argumentFunction){
    argumentFunction();
    
    console.log("This is code comes from invokeFunction");
}

//argumentFunction();

invokeFunction(argumentFunction);


invokeFunction(argumentFunction);

function createFullName(firstName, middleName, lastName){
    console.log(firstName+" "+middleName+" "+lastName);
}

createFullName('Juan', 'Dela');
createFullName('Juan', 'Dela', 'Cruz');
createFullName('Juan', 'Dela', 'Cruz', 'Hello');

let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

//return statements

function returnFullname(firstName, middleName, lastName){
    
    
    return firstName+" "+middleName+" "+lastName;

    console.log("this message will not be printed");
}

console.log(returnFullname('Joe', 'Jayson', 'Helberg'));