/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

//1 addition
function addNumbers(num1, num2){
    console.log(`The sum of ${num1} and ${num2} is ${(num1+num2)}`);
}
addNumbers(1,3);

//1 subtract
function subtractNumbers(num1, num2){
    console.log(`The difference of ${num1} and ${num2} is ${(num1-num2)}`);
}
subtractNumbers(3,1);

//2 multiply
function multiplyNumbers(num1, num2){
    return `The product of ${num1} and ${num2} is ${(num1*num2)}`;
}

let product = multiplyNumbers(1,3);
console.log(product);

//2 divide
function divideNumbers(num1, num2){
    return `The quotient of ${num1} and ${num2} is ${(num1/num2)}`;
}
let quotient = divideNumbers(1,3);
console.log(quotient);

//3 area of circle
function circle(radius){
    let area = Math.PI*Math.pow(radius, 2);
    return `The area of the circle with the radius ${radius} area is ${area}`;
}

let circleArea = circle(5);
console.log(circleArea);

//4 average

function getAverage(num1, num2, num3, num4){
    let sum = num1+num2+num3+num4;
    let average = sum/4;
    return `The average of the numbers: ${num1}, ${num2}, ${num3}, and ${num4} is ${average}`;
}

let average = getAverage(5,6,7,12);
console.log(average);

//5 ispassed
function isPassed(yourScore, total){
    let score = yourScore/total;
    let ispassed = (score >= .75);
    console.log(`is ${yourScore}/${total} a passing score?`)
    return ispassed;
}

console.log(isPassed(75,100));